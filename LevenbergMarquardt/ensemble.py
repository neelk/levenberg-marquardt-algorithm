#!/usr/bin/env python3


import sys

from collections import defaultdict
import numpy as np


class Booster(object):

    def __init__(self, estimators):
        self.estimators = estimators
        self.n_estimators = len(estimators)
        self.weights = [1/self.n_estimators
                        for i in range(self.n_estimators)]

    def __predict(self, x):
        individual_outputs = [self.estimators[i].predictSinglePoint(x)
                              for i in range(self.n_estimators)]
        votes = defaultdict(int)
        for i in range(len(individual_outputs)):
            votes[individual_outputs[i]] += self.weights[i]
        predicted = None
        for key in votes:
            if predicted is None:
                predicted = key
            else:
                if votes[predicted] < votes[key]:
                    predicted = key
        return key

    def predict(self, inputs):
        outputs = np.zeros((len(inputs),))
        for i in range(len(inputs)):
            outputs[i] = self.__predict(inputs[i])
        return outputs

    def fit(self, fit_args):
        if len(fit_args) != self.n_estimators:
            err = 'Number of argument dicts passed in fit_args not' \
                  'equal to number of estimators'
            raise ValueError(err)
        for i in range(self.n_estimators):
            self.estimators[i].fit(**fit_args[i])

#!/usr/bin/env python3


import sys
from datetime import datetime

import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler as SC
import sklearn.metrics as metrics

import LevenbergMarquardt.NN as LM
import LevenbergMarquardt.ensemble as ensemble


def main(argv):

    if len(argv) > 1:
        seed = int(argv[1])
    else:
        seed=0

    #### DATA PREPROCESSING ####

    input_attr = ['age', 'male', 'female', 'tb', 'db', 'alkphos', 'sgpt',
                  'sgot', 'tp', 'alb', 'ag']

    df = pd.read_csv('Data/dataset.csv')
    X = df[input_attr]
    y = df['truth_value']

    X_train, X_test, y_train, y_test = \
        train_test_split(X, y, test_size=0.20, random_state=0)

    sc = SC()
    X_train = sc.fit_transform(X_train)
    X_test = sc.fit_transform(X_test)

    X_train, y_train, X_test, y_test = np.array(X_train),\
        np.array(y_train), np.array(X_test), np.array(y_test)
    y_train = np.reshape(y_train, (len(y_train), 1))
    y_test = np.reshape(y_test, (len(y_test), 1))

    print(X_train.shape, y_train.shape)
    print(X_test.shape, y_test.shape)

    #################################################################

    #### DEFINING THE NEURAL NETWORKS ####

    n_estimators = 5
    seeds = [0,3,5,8,9]

    layers = []
    for i in range(n_estimators):
        layers.append((LM.FullyConnectedLayer(11, 6, seed=seeds[i]),
                      LM.FullyConnectedLayer(6, 2)))

    models = [LM.NN(layers[i], trainType='gd', lr=0.01)
              for i in range(n_estimators)
             ]

    fit_args = {'inputs':X_train, 'targets':y_train, 'epochs':7}

    booster = ensemble.Booster(models)

    startTime = datetime.now()
    booster.fit([fit_args] * n_estimators)
    print('Train execution time = ', datetime.now() - startTime)

    #################################################################

    #### TESTING ####

    print('Testing...')
    startTime = datetime.now()
    y_pred = booster.predict(X_test)
    print('Test execution time = ', datetime.now() - startTime)

    y_pred_labels = list(y_pred > 0.5)

    for i in range(len(y_pred_labels)):
        if int(y_pred_labels[i]) == 1:
            y_pred_labels[i] = 1
        else:
            y_pred_labels[i] = 0


    cm = metrics.confusion_matrix(y_test, y_pred_labels)
    print("\n")
    print("Confusion Matrix : ")
    print(cm)

    #################################################################

    return 0


if __name__ == '__main__':
    sys.exit(main(sys.argv))
